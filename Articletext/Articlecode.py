#!/usr/bin/python
# -*- coding: utf-8 -*-

# f = open(r'TextArticle')
# lines = f.readlines()
# f.close()
#
# lines[0] = '<h1>' + lines[0] + '</h1>' + '\n'
# lines[2] = '<p>' + lines[2] + '</p>' + '\n'
# f = open(r'TextArticle', 'w')
# f.writelines(lines)
# f.close()
# ~~~~~~~~方法1

import re
import fileinput


def LineCount():
    return len(open('TextArticle').readlines())


def leftTrim(line, char):
    another = ''
    l = len(line)

    # Method 3~~~~~~~~~~~
    for i in range(l):
        if line[i] != char:
            break
    another = line[i:l]
    return another

    # Method 1~~~~~~~~~~~~
    # first = 99999
    # for i in range(l):
    #     if line[i] != ' ':
    #         another += line[i]
    #         if first == 99999:
    #             first = i
    #
    #     if line[i] == ' ' and i > first:
    #         another += line[i]

    # Method 1-1~~~~~~~~~~~~~~
    # for i in range(l):
    #     if line[i] != ' ':
    #         break
    # for j in range(i,l,1):
    #     another += line[j]

    # Method 2~~~~~~~~~~~~~
    # boolFirst = False
    # for i in range(l):
    #     if line[i] != ' ':
    #         another += line[i]
    #         boolFirst = True
    #
    #     if line[i] == ' ' and boolFirst == True:
    #         another += line[i]


def trim(line, char):
    result = line
    l = len(result)
    for i in range(l):
        if result[i] != char:
            result = result[i:l]
            break

    l = len(result)
    for i in range(l - 1, -1, -1):
        if result[i] != char:
            result = result[0: i + 1]
            break

    return result

def combineDoubleLine(line, line2):
    line2 = leftTrim(line2, ' ')
    result = trim(line, '\n') + ' ' + line2
    return result


def mark(k1, line, k2):
    result = trim(line, '\n')
    return k1 + result + k2


def fuction(outputfile, inputfile):
    f = open(outputfile)
    lines = f.readlines()
    lc = LineCount()
    k = 0
    for i in range(lc):
        if i >= lc:
            break
        if lines[k] != '\n':
            lines[k] = leftTrim(lines[k], ' ')
            lines[k] = lines[k].replace('<', '&lt;').replace('>', '&gt;')
            blank = 0

            if lines[k] == lines[-1]:
                pass

            else:
                if lines[k] != '\n' and lines[k + 1] != '\n':
                    lines[k]= combineDoubleLine(lines[k], lines[k + 1])
                    del lines[k + 1]
                    lc -= 1

            if k == 0:
                lines[k] = mark('<h1>', lines[k], '</h1>') + '\n'

            else:
                if lines[k] == lines[-1]:
                    lines[k] = mark('<p>', lines[k], '</p>')
                else:
                    lines[k] = mark('<p>', lines[k], '</p>') + '\n'
        else:
            if blank > 0:
                del lines[k]
                k -= 1
            blank += 1
        k += 1

    f.close()

    f1 = open(inputfile, 'w')
    f1.writelines(lines)
    f1.close()


fuction(r'TextArticle', r'TextArticleInput')