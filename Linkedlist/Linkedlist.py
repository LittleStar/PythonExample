# !/usr/bin/python
# -*- coding: UTF-8 -*-

class Node:
    def __init__(self, data):
        self.data = data
        # self.prev = None
        self.next = None


class Linkedlist:
    def __init__(self):
        self.head = None
        self.tail = None

    def is_empty(self):
        return print('The list is empty list:', self.head is None)

    def append(self, data):
        node = Node(data)
        if self.head is None:
            self.head = node
            self.tail = node
        else:
            self.tail.next = node
            self.tail = node

    def iter(self):
        if not self.head:
            return
        cur = self.head
        while cur:
            yield cur.data
            cur = cur.next

    def insert(self, idx, value):
        node = Node(value)
        cur = self.head
        cur_idx = 0
        if cur is None:
            raise Exception('The list is an empty list')

        if idx < 0:
            raise Exception('The index can not be less than zero')

        if idx == 0:
            node.next = cur
            self.head = node
            return

        while cur_idx < idx - 1:
            cur = cur.next
            if cur is None:
                raise Exception('List length less than index')
            cur_idx += 1

        node.next = cur.next
        cur.next = node
        if node.next is None:
            self.tail = node

    def remove(self, idx):
        cur = self.head
        cur_idx = 0
        if self.head is None:
            raise Exception('The list is an empty list')
        while cur_idx < idx - 1:
            cur = cur.next
            if cur is None:
                raise Exception('list length less than index')
            cur_idx += 1
        if idx == 0:
            self.head = cur.next
        else:
            cur.next = cur.next.next

        if cur.next is None:
            self.tail = cur

    def size(self):
        current = self.head
        count = 0
        if current is None:
            return 'The list is an empty list'
        while current is not None:
            count += 1
            current = current.next
        return count

    # def search(self, item):
    #     current = self.head
    #     found = False
    #     while current is not None and not found:
    #         if current.data == item:
    #             found = True
    #         else:
    #             current = current.next
    #     return found


    def search(self, item):
        cur = self.head
        getcount = self.size()
        for i in range(getcount):
            if cur.data == item:
                print('The index of the search is:', i)
                return
            else:
                cur = cur.next
                i += 1
        print('Not found')
        return

    def traverse(self):
        lst = []
        for data in self.iter():
            lst.append(data)
        return lst


if __name__ == '__main__':
    link_list = Linkedlist()
    for i in range(4):
        link_list.append(i)
        # print(link_list.is_empty())
    link_list.insert(4, 'b')
    print(link_list.traverse())

    # link_list.remove(0)
    # print(link_list.traverse())

    print('Len of the list is:', link_list.size())
    link_list.search(0)
