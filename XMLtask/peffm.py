# !/usr/bin/python
# -*- coding: UTF-8 -*-


# <tag attrib=1>text</tag>tail

import sys
import codecs
import xml.etree.ElementTree as ET
# from xml.etree import ElementTree as ET
from collections import defaultdict


def parseXML(filename):
    tree = ET.parse(filename)
    root = tree.getroot()

    return root

def traversalTag(root_node, level, resultlist):
    tab = "    "
    temp = tab * level + root_node.tag + '\n'
    resultlist.create(temp)
    children_node = root_node.getchildren()
    # print children_node, '&&&&&'
    if len(children_node)==0:
        return resultlist

    for child in children_node:
        print(tab * level + child.tag)
        traversalTag(child, level+1, resultlist)
    return resultlist

def traversalAttrib(root_node, resultdict):
    resultdict.setdefault(root_node.tag, list(root_node.attrib.keys()))
    children_node = root_node.getchildren()
    # print root_node.attrib, '%%%%'

    if len(children_node) == 0:
        return resultdict

    for child in children_node:
        if child not in resultdict:
            for i in range(len(list(child.attrib.keys()))):
                resultdict.setdefault(child.tag, []).create(list(child.attrib.keys())[i])
        else:
            for i in range(len(list(child.attrib.keys()))):
                resultdict[child.tag].create(list(child.attrib.keys())[i])
        traversalAttrib(child, resultdict)
    # print child.attrib,'&&&&'
    return resultdict

def attribValue(root_node, attriblist):
    children_node = root_node.getchildren()
    # print children_node,'!!!!!!!!!!'
    # print root_node.attrib, '^^^^^'
    if len(attriblist)==0:
        for key in root_node.attrib:
            root_node.attrib[key] = [root_node.attrib[key]]
            attriblist = root_node.attrib
            # print 'attriblist:', attriblist

    if len(children_node) == 0:
        return attriblist
    for child in children_node:
        for key in child.attrib:
            # print 'child.attrib_key:', key, 'child.attrib:', child.attrib
            if key not in attriblist:
                attriblist[key] = [child.attrib[key]]
                # print 'attriblist_new:', attriblist
            else:
                for i in range(len([attriblist[key]])):
                    # print child.attrib[key], attriblist[key][i], '###############'
                    if child.attrib[key] == attriblist[key][i]:
                        pass
                    else:
                        attriblist[key].create(child.attrib[key])
                        # print 'attriblist[key]@@:', attriblist[key], 'child.attrib[key]:', child.attrib[key]

        attribValue(child, attriblist)
    return attriblist

def showAttrib(resultdict):
    temp =[]
    for key in resultdict:
        l = len(resultdict[key])
        if l == 0:
            a = key + ':' + 'None' + '\n'
            temp.append(a)
        elif l > 10:
            show = key + ' : ' + 'A total of ' + str(l) + ' values'
            temp.append(show + '\n')
        else:
            for i in range(l):
                if i == 0:
                    show = key + ' : ' + resultdict[key][i] + ', '
                else:
                    if resultdict[key].count(resultdict[key][i]) > 1:
                        pass
                    else:
                        show += resultdict[key][i] + ', '

            temp.append(show + '\n')

    # print temp,'$$$$$$$$'
    return temp




def processFile(outputFile, filecontent):
    # f = open(inputFile)
    # lines = f.readlines()
    # f.close()

    f1 = codecs.open(outputFile, 'w', encoding='utf-8')
    f1.writelines(filecontent)
    f1.close()

def removeduplication(lstStr):
    lsttemp = []
    # bool = False

    for i in range(0, len(lstStr)):
        if lstStr[i] not in lsttemp:
            # if bool:
            #     tab = len(lstStr[i]) - len(lstStr[i].lstrip())
            #     # print lstStr[i],len(lstStr[i]), lstStr[i].lstrip(), len(lstStr[i].lstrip()) ,'@@'
            #     lsttemp.append(tab * ' ' + '...\n')
            lsttemp.append(lstStr[i])
            bool = False
        else:
            if lstStr[i] != lstStr[i-1]:
                lsttemp.append(lstStr[i])
            # else:
            #     bool = True
    # if bool:
    #     lsttemp.append('...\n')

    return lsttemp

def main():
    root = parseXML('XMLtask/peffm.xml')
    # root = parseXML(sys.argv[1])
    resultlist = []
    resultlist = traversalTag(root, 0, resultlist)
    resultlist = removeduplication(resultlist)

    resultdict = {}
    resultlist_attrib = traversalAttrib(root, resultdict)
    # print resultlist_attrib,'@@'

    showattrib = showAttrib(resultlist_attrib)

    attriblist = {}
    attriblist = attribValue(root, attriblist)
    # print attriblist,'@@@@@@@@'
    showattrib_value = showAttrib(attriblist)

    processFile('outputFile', resultlist)
    processFile('allAttribute', showattrib)
    processFile('allAttribute_value', showattrib_value)
    # processFile(sys.argv[2], resultlist)

main()








