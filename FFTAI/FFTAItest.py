def insertNa(lines):
    lc = len(lines)

    for i in range(lc):
        if 'using' not in lines[i]:
            lines.insert(i + 1, '\n')
            lines.insert(i + 2, '\n')
            # lines.insert(i+3, '\n')
            lines[i + 1] = 'namespace FFTAI.Halley\n'
            lines[i + 2] = '{\n'
            return

def deluFftaiH(lines):
    lc = len(lines)
    for i in range(lc):
        if 'using FFTAI.Halley' in lines[i]:
            del lines[i]
            return


def rightMove(lines):
    lc = len(lines)

    for i in range(lc):
        if lines[i] == '{\n':
            for k in range(i + 1, lc):
                lines[k] = '    ' + lines[k]
            return

def delLf(lines):
    lc = len(lines)
    for i in range(lc-1, -1, -1):
        str = lines[i].strip(' ')
        if str == '\r\n':
            del lines[i]

        if str == '}\r\n':
            lines[i] = lines[i].strip('\r\n')
            return

        if str == '}':
            return

def isNamespace(lines):
    lc = len(lines)

    for i in range(lc):
        if 'namespace' in lines[i]:
            return True
            break

    return False


def processFile(inputFile, outputFile):
    # f = open(inputFile, 'r', encoding='utf-8', errors='ignore')
    f = open(inputFile)
    lines = f.readlines()
    f.close()

    if isNamespace(lines):
        f1 = open(outputFile, 'w')
        f1.writelines(lines)
        f1.close()

    else:
        insertNa(lines)
        deluFftaiH(lines)
        rightMove(lines)
        delLf(lines)
        lines.append('\r\n}')

    f1 = open(outputFile, 'w')
    f1.writelines(lines)
    f1.close()


# processFile('FFTAInamespaceInput', 'FFTAInamespaceOutput')


import os
import sys
# import codecs

count = 0
c = 0
path = '/Users/Star/PycharmProjects/MarkupApplication/Scripts'
path2 = '/Users/Star/PycharmProjects/MarkupApplication/cc/dd/Scripts2'

path = sys.argv[1]
path2 = sys.argv[2]


if not os.path.exists(path2):
    os.makedirs(path2, 0o777)

for (dirPath, dirs, files) in os.walk(path):
    # print dirPath # 目录的路径
    # print dirs    # 路径下的目录
    # print files, '\n'  # 路径下的文件
    # c += len(files)
    # print c, '###'

    for filename in files:
        if filename.endswith('.cs'):
            fullPath = os.path.join(dirPath, filename) # 用/连接目录路径和文件
            # dirP = dirPath + '/'
            # fullPath = dirP + filename
            subDir = dirPath.split(path)[1]

            moveT = path2 + subDir
            if not os.path.exists(moveT):
                os.makedirs(moveT, 0o777)
            moveToPath = os.path.join(moveT, filename)


            processFile(fullPath, moveToPath)
            count += 1

print('Files:', count)
print('Complete')