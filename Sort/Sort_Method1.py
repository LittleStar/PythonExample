import os
# import sys
#
# path = '/Users/Star/PycharmProjects/MarkupApplication/SystemEnvironmentVariable'
# path_copy = '/Users/Star/PycharmProjects/MarkupApplication/SystemEnvironmentVariable_copy'
# path = sys.argv[1]
# path_copy = sys.argv[2]

sortkey = ['\n', '\r', ' ', '_', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G',
           'H', 'I', 'G', 'K','L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c',
           'd', 'e', 'f', 'g', 'h', 'i', 'g', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 's', 'y',
           'z']


def sortCmp(left, right):
    l = len(sortkey)
    try:
        x = sortkey.index(left)
    except ValueError:
        x = l

    try:
        y = sortkey.index(right)
    except ValueError:
        y = l

    if x > y:
        return 1
    elif x == y:
        return 0
    elif x < y:
        return -1


def processFile(inputFile, outputFile):
    f = open(inputFile)
    lines = f.readlines()
    f.close()
    sortlines(lines, sortCmp)
    f1 = open(outputFile, 'w')
    f1.writelines(lines)
    f1.close()


# def sortlines(lines, compare):
#     l = len(lines)
#     for i in range(0, l - 1):
#         for j in range(i + 1, l):
#             result = sortl(lines, i, j, compare)
#             if result == True:
#                 lines[i], lines[j] = lines[j], lines[i]
#
#
# def sortl(lst, leftIndex, rightIndex, compare):
#     str1 = lst[leftIndex].split('=')[0]
#     str2 = lst[rightIndex].split('=')[0]
#     l1 = len(str1)
#     l2 = len(str2)
#     i = 0
#     j = 0
#     while True:
#         if i < l1 and j < l2:
#             result = compare(str1[i], str2[j])
#             print result,'@@@@'
#             if result == 1:
#
#                 return True
#             elif result == 0:
#                 i += 1
#                 j += 1
#             elif result == -1:
#                 break
#         else:
#             if l1 > l2:
#                 return True
#             break



def sortlines(lines, compare):
    l = len(lines)
    for i in range(0, l - 1):
        for j in range(i + 1, l):
            sortl(lines, i, j, compare)


def sortl(lst, leftIndex, rightIndex, compare):
    str1 = lst[leftIndex].split('=')[0]
    str2 = lst[rightIndex].split('=')[0]
    l1 = len(str1)
    l2 = len(str2)
    i = 0
    j = 0
    while True:
        if i < l1 and j < l2:
            result = compare(str1[i], str2[j])
            if result == 1:
                lst[leftIndex], lst[rightIndex] = lst[rightIndex], lst[leftIndex]
                break
            if result == 0:
                i += 1
                j += 1
            if result == -1:
                break
        else:
            if l1 > l2:
                lst[leftIndex], lst[rightIndex] = lst[rightIndex], lst[leftIndex]
            break


processFile('Origin', 'output')
# processFile(path, path_copy)
