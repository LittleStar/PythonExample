sortkey = ['\n', '\r', ' ', '_', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G',
           'H', 'I', 'G', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c',
           'd', 'e', 'f', 'g', 'h', 'i', 'g', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 's', 'y',
           'z']


def sortCmp(lst1, lst2):
    l = len(sortkey)

    str1 = lst1.split('=')[0]
    str2 = lst2.split('=')[0]
    l1 = len(str1)
    l2 = len(str2)

    i = 0
    j = 0
    while True:
        if i < l1 and j < l2:
            try:
                x = sortkey.index(str1[i])
            except ValueError:
                x = l

            try:
                y = sortkey.index(str2[j])
            except ValueError:
                y = l

            if x > y:
                return 1
            elif x == y:
                i += 1
                j += 1
            elif x < y:
                break
        else:
            if l1 > l2:
                return 1
            break


def processFile(inputFile, outputFile):
    f = open(inputFile)
    lines = f.readlines()
    f.close()
    sortlines(lines, sortCmp)
    f1 = open(outputFile, 'w')
    f1.writelines(lines)
    f1.close()


def sortlines(lines, compare):
    l = len(lines)
    for i in range(0, l - 1):
        for j in range(i + 1, l):
            result = compare(lines[i], lines[j])
            if result == 1:
                lines[i], lines[j] = lines[j], lines[i]


processFile('Origin', 'output')
