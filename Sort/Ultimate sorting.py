cols = [
    'Aw H',
    'Hm I1',
    'Aw I2',
    'Hm R',
    'Aw R',
    'Aw I1',
    'Aw E',
    'Hm I2',
    'Hm H',
    'Hm T',
    'Hm E',
]


#  Aw > Hm

# I > R > H > E

def sortCols():
    global cols
    le = len(cols)
    col1 = []
    col2 = []
    for i in range(le):
        sp = cols[i].split()
        col1.append(sp[0])
        col2.append(sp[1])

    mySort(col1, col2, myCmpAH)
    subSort(col1, col2, myCmp2)

    result = []
    for i in range(le):
        result.append(col1[i] + ' ' + col2[i])

    print 'cols:', cols
    print 'Sorted:', result


def mySort(sortlst, sublst, compare):
    le = len(sortlst)
    for i in range(le - 1):
        for j in range(i + 1, le):
            result = compare(sortlst[i], sortlst[j])
            if result == 1:
                # lst[i] > lst[j]
                sortlst[i], sortlst[j] = sortlst[j], sortlst[i]
                sublst[i], sublst[j] = sublst[j], sublst[i]


def subSort(mainlst, sortlst, compare=cmp):
    le = len(sortlst)
    for i in range(le - 1):
        for j in range(i + 1, le):
            if mainlst[i] == mainlst[j]:
                result = compare(sortlst[i], sortlst[j])
                if result == 1:
                    sortlst[i], sortlst[j] = sortlst[j], sortlst[i]
                    mainlst[i], mainlst[j] = mainlst[j], mainlst[i]


def myCmpAH(left, right):
    if left == 'Aw' and right == 'Hm':
        return 1
    elif left == right:
        return 0
    elif left == 'Hm' and right == 'Aw':
        return -1

    return None


def myCmp2(left, right):
    lst = ['E', 'H', 'R', 'I1', 'I2']
    maxValue=len(lst)
    try:
        x = lst.index(left)
    except ValueError:
        x=maxValue

    try:
        y = lst.index(right)
    except ValueError:
        y=maxValue

    # x= getIndex(lst,left)
    if x > y:
        return 1
    elif x < y:
        return -1
    elif x == y:
        return 0
        # l=eval(left[1])
        # r=eval(right[1])
        # if l>r:
        #     return 1
        # elif l<r:
        #     return -1
        # else:
        #     return 0

    return None


def getIndex(lst, value):
    for i in range(len(lst)):
        if lst[i] == value:
            return i

    return None


sortCols()