import sys

path = sys.argv[1]
# path2 = sys.argv[2]

if len(sys.argv) < 2:
    print('No data file specified!')
    sys.exit()

# if len(sys.argv) < 3: #  若想用这种输出文件方式,那就需要把又error提示信息的变量添加到lst列表里面,然后作为writelines的参数使用
#     print('No output file specified!')
#     sys.exit()

if len(sys.argv) < 3:  # 若使用上一条件,则2->4
    rangevalue = 5
    print('Sequence max number is unset.Using default value.')
else:
    rangevalue = int(sys.argv[2])  # 若使用上一条件,则2->3
print('Squence max number is ', rangevalue)

linecount = 0
current = 1


def readoneline(inputfile):
    for line in inputfile:
        yield line


def refreshCurrent(number, rangevalue):
    if number > rangevalue:
        number = 1
    return number


def check(data):
    global current

    if data == current:
        # current += 1
        # if current > rangevalue:
        #     current = 1
        current = refreshCurrent(current + 1, rangevalue)
        return True

    elif data == current - 1 or (data == rangevalue and current == 1):  # or后面的是中间数据为最大值的情况
        return True

    else:
        # current = data + 1
        # if current > rangevalue:
        #     current = 1
        current = refreshCurrent(data + 1, rangevalue)
        return False


if __name__ == '__main__':
    isnoerrors = True
    onefile = open(path)
    for oneline in readoneline(onefile):
        linecount += 1
        data = int(oneline)
        if linecount == 1 and data != 1:  # 判断的是开始数据不等于1的情况(并且有可能为最大值,要返回错误信息)
            print('There is an error!line:', linecount)
            # current = data + 1
            # if current > rangevalue:
            #     current == 1
            current = refreshCurrent(data + 1, rangevalue)
            continue

        if check(data):
            continue
        else:
            print('There is an error!line:', linecount)
            isnoerrors = False

    if isnoerrors:
        print('No errors!')
    print('Done!')


    # onefile2 = open(path2, 'w')
    # onefile2.writelines() #  若想用这种输出文件方式,那就需要把又error提示信息的变量添加到lst列表里面,然后作为writelines的参数使用
    # onefile2.close()
