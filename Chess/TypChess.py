def putQueen(allQueenPos, x, y):
    # Significant Error!!
    # for queen in allQueenPos:
    #     if queen == (-1, -1):
    #         queen = (x, y)
    #         break

    for i in range(len(allQueenPos)):
        if allQueenPos[i] == (-1, -1):
            allQueenPos[i] = (x, y)
            break # 保证当前的xy的值只赋给一个皇后位置??
    return


def canPut(queenPos, x, y):
    # 可删除
    if queenPos[0] == (-1, -1):
        return True

    for queen in queenPos:
        if queen != (-1, -1): # 又判断一次?
            if x == queen[0] or y == queen[1]:
                return False
            else:
                if abs(x - queen[0]) == abs(y - queen[1]):
                    return False

    return True


def countQueens(allQueenPos):
    count = 0
    for queen in allQueenPos:
        if queen != (-1, -1):
            count += 1

    return count


def recurse(allQueenPos, x, y):
    if countQueens(allQueenPos) == 4:
        return

    if canPut(allQueenPos, x, y):
        putQueen(allQueenPos, x, y)
        if countQueens(allQueenPos) == 4:
            return
        else:
            for i in range(x + 1, 4):
                for j in range(0, 4):
                    recurse(allQueenPos, i, j)
    return


def fourQueen():
    # Method 1
    # chessBoard = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]
    # Method 2
    # chessBoard = [[0 for _ in range(4)] for _ in range(4)]

    for i in range(4):
        for j in range(4):
            allQueenPos = [(-1, -1) for _ in range(4)]
            recurse(allQueenPos, i, j)
            if countQueens(allQueenPos) == 4:
                print(allQueenPos)


# Main Entry
fourQueen()
