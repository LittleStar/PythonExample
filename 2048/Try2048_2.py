# -*- coding:utf-8 -*-

import curses
from random import randrange, choice
from collections import defaultdict

# ord将字符串转换成ascii码
letter_codes = [ord(ch) for ch in 'WASDRQwasdrq']
actions = ['Up', 'Left', 'Down', 'Right', 'Restart', 'Exit']
# 创建一个字典(由ascii码和actions组成) 例如zip=[(87,'Up'),...],dict={87:'UP',...}
actions_dict = dict(list(zip(letter_codes, actions * 2)))


def transpose(field):
    return [list(row) for row in zip(*field)]


# 用户输入处理,阻塞 + 循环,知道获得用户有效输入才返回对应行为
def get_user_action(keyboard):
    char = 'N'
    while char not in actions_dict:
        char = keyboard.getch()
    return actions_dict[char]


# 创建棋盘
class GameField(object):  # object是新式类
    def __init__(self, height=4, width=4, win=2048):
        self.height = height
        self.width = width
        self.win_value = win
        self.score = 0  # 当前分数
        self.highscore = 0  # 最高分
        self.reset()  # 棋盘重置

    def reset(self):
        if self.score > self.highscore:
            self.highscore = self.score
        self.score = 0
        self.field = [[0 for i in range(self.width)] for j in range(self.height)]  # 二维数组 [[0,0,0,0],[0,0,0,0],..]
        self.spawn()
        self.spawn()  # 初始化两个值,所以写两遍

    def move(self, direction):
        def move_row_left(row):
            def tighten(row):  # 把零散的非零单元挤到一块
                new_row = [i for i in row if i != 0]
                new_row += [0 for i in range(len(row) - len(new_row))]
                return new_row

            def merge(row):  # 对邻近元素进行合并
                pair = False
                new_row = []
                for i in range(len(row)):
                    if pair:
                        new_row.append(2 * row[i])
                        self.score += 2 * row[i]
                        pair = False
                    else:
                        if i + 1 < len(row) and row[i] == row[i + 1]:
                            pair = True
                            new_row.append(0)  # 确定可以合并后使当前的元素为0,下一个元素合并后的值
                        else:
                            new_row.append(row[i])
                assert len(new_row) == len(row)
                return new_row
                # 先挤到一块再合并再挤到一块

            return tighten(merge(tighten(row)))

        def move_row_right(row):

            def tighten(row):
                temp = [i for i in row if i != 0]
                new_row = [0 for i in range(len(row) - len(temp))]
                new_row += temp
                return new_row

            def merge(row):  # 对邻近元素进行合并
                pair = False
                new_row = []
                for i in range(len(row) - 1, -1, -1):
                    if pair:
                        new_row.append(2 * row[i])
                        self.score += 2 * row[i]
                        pair = False
                    else:
                        if i - 1 > -1 and row[i] == row[i - 1]:
                            pair = True
                            new_row.append(0)  # 确定可以合并后使当前的元素为0,下一个元素合并后的值
                        else:
                            new_row.append(row[i])
                new_row.reverse()
                assert len(new_row) == len(row)
                return new_row
                # 先挤到一块再合并再挤到一块

            return tighten(merge(tighten(row)))

        def move_row_up(row):
            def tighten(row):
                new_row = [i for i in row if i != 0]
                new_row += [0 for i in range(len(row) - len(new_row))]
                return new_row

            def merge(row):  # 对邻近元素进行合并
                pair = False
                new_row = []
                for i in range(len(row)):
                    if pair:
                        new_row.append(2 * row[i])
                        self.score += 2 * row[i]
                        pair = False
                    else:
                        if i + 1 < len(row) and row[i] == row[i + 1]:
                            pair = True
                            new_row.append(0)  # 确定可以合并后使当前的元素为0,下一个元素合并后的值
                        else:
                            new_row.append(row[i])
                assert len(new_row) == len(row)
                return new_row
                # 先挤到一块再合并再挤到一块

            return tighten(merge(tighten(row)))

        def move_row_down(row):
            def tighten(row):
                temp = [i for i in row if i != 0]
                new_row = [0 for i in range(len(row) - len(temp))]
                new_row += temp
                return new_row

            def merge(row):  # 对邻近元素进行合并
                pair = False
                new_row = []
                for i in range(len(row) - 1, -1, -1):
                    if pair:
                        new_row.append(2 * row[i])
                        self.score += 2 * row[i]
                        pair = False
                    else:
                        if i - 1 > -1 and row[i] == row[i - 1]:
                            pair = True
                            new_row.append(0)  # 确定可以合并后使当前的元素为0,下一个元素合并后的值
                        else:
                            new_row.append(row[i])
                new_row.reverse()
                assert len(new_row) == len(row)
                return new_row
                # 先挤到一块再合并再挤到一块

            return tighten(merge(tighten(row)))

        moves = {}
        # 为字典键为Left赋值,move_row_left(row)返回的是new_row(一个列表),循环之后就是由四个列表组成的列表
        moves['Left'] = lambda field: [move_row_left(row) for row in field]
        moves['Right'] = lambda field: [move_row_right(row) for row in field]
        moves['Up'] = lambda field: transpose([move_row_up(row) for row in transpose(field)])
        moves['Down'] = lambda field: transpose([move_row_down(row) for row in transpose(field)])

        if direction in moves:
            # self.log(direction)   # 和log有关
            if self.move_is_possible(direction):
                self.log('move is possible')
                self.field = moves[direction](self.field)
                self.spawn()
                return True
            else:
                self.log('move is false')
                return False

    # 判断输赢
    def is_win(self):
        return any(any(i >= self.win_value for i in row) for row in self.field)

    def is_gameower(self):
        return not any(self.move_is_possible(move) for move in actions)

    def log(self, string):
        return  # 打了return就不用注掉log了
        global scr
        scr.addstr('********** ' + string + '\n')
        scr.refresh()

    # 绘制棋盘
    def draw(self, screen):
        help_string1 = '(W)Up (S)Down (A)Left (D)Right'
        help_string2 = '     (R)Restart (Q)Exit'
        gameover_string = '           GAME OVER'
        win_string = '          YOU WIN!'

        def cast(string):
            screen.addstr(string + '\n')
            # screen.refresh()

        def draw_hor_separator():
            line = '+' + ('+------' * self.width + '+')[1:]
            separator = defaultdict(lambda: line)  # 生成子类化字典的实例
            if not hasattr(draw_hor_separator, 'counter'):
                draw_hor_separator.counter = 0
            cast(separator[draw_hor_separator.counter])  # cast变量string(separator[0]),也就是separator默认值line
            draw_hor_separator.counter += 1  # 只要有row在field里面,每一行都显示默认值lien的值

        # 当num不为0时放在||中间,为0时直接画出|(六个字符)|
        def draw_row(row):
            cast(''.join('|{: ^5} '.format(num) if num > 0 else '|      ' for num in row) + '|')

        screen.clear()  # 清屏(防止第二次生成棋盘)
        cast('SCORE: ' + str(self.score))  # 显示成绩
        if self.highscore != 0:
            cast('HIGHSCORE: ' + str(self.highscore))

        for row in self.field:
            draw_hor_separator()
            draw_row(row)
        draw_hor_separator()

        if self.is_win():
            cast(win_string)
        else:
            if self.is_gameower():
                cast(gameover_string)
            else:
                cast(help_string1)

        cast(help_string2)

    def spawn(self):
        new_element = 4 if randrange(100) > 89 else 2  # 生成4是10/100, 2是90/100
        # 当元素格为0时,随机产生二维数组赋值给ij
        (i, j) = choice([(i, j) for i in range(self.width) for j in range(self.height) if self.field[i][j] == 0])
        self.field[i][j] = new_element

    def move_is_possible(self, direction):

        def row_is_left_movable(row):

            def change(i):
                if row[i] == 0 and row[i + 1] != 0:  # 可以移动
                    return True
                if row[i] != 0 and row[i + 1] == row[i]:  # 可以合并
                    return True
                return False

            return any(change(i) for i in range(len(row) - 1))

        def row_is_right_movable(row):

            def change(i):
                if row[i] == 0 and row[i - 1] != 0:
                    return True
                if row[i] != 0 and row[i - 1] == row[i]:
                    return True
                return False

            return any(change(i) for i in range(len(row) - 1, 0, -1))

        def row_is_up_movable(row):

            def change(i):
                if row[i] == 0 and row[i + 1] != 0:
                    return True
                if row[i] != 0 and row[i + 1] == row[i]:
                    return True
                return False

            return any(change(j) for j in range(len(row) - 1))

        def row_is_down_movable(row):

            def change(i):
                if row[i] == 0 and row[i - 1] != 0:
                    return True
                if row[i] != 0 and row[i - 1] == row[i]:
                    return True
                return False

            return any(change(j) for j in range(len(row) - 1, 0, -1))

        check = {}
        check['Left'] = lambda field: any(row_is_left_movable(row) for row in field)
        check['Right'] = lambda field: any(row_is_right_movable(row) for row in field)
        check['Up'] = lambda field: any(row_is_up_movable(row) for row in transpose(field))
        check['Down'] = lambda field: any(row_is_down_movable(row) for row in transpose(field))

        if direction in check:
            self.log('into check')
            return check[direction](self.field)  # 返回True(若可移动或可合并) 返回false(若不可移动且不可合并)
        else:
            return False  # False(不可移动或者不可合并)


scr = None   # 和打log有关


def main(stdscr):
    def init():
        # 重置游戏棋盘
        game_field.reset()
        return 'Game'  # 和state_actions有关

    def not_game(state):
        # 画出GameOver 或者 Win的界面
        game_field.draw(stdscr)
        # 读取用户输入得到action, 判断是重启游戏还是结束游戏
        action = get_user_action(stdscr)
        # 如果action不在responses字典里,就返回默认值state
        responses = defaultdict(lambda: state)
        responses['Restart'], responses['Exit'] = 'Init', 'Exit'
        return responses[action]

    def game():
        # 画出当前棋盘状态
        game_field.draw(stdscr)
        # 读取用户输入得到action
        action = get_user_action(stdscr)

        if action == 'Restart':
            return 'Init'
        if action == 'Exit':
            return 'Exit'
        if game_field.move(action):  # 成功移动了一步
            if game_field.is_win():  # 游戏胜利了
                return 'Win'
            if game_field.is_gameower():  # 游戏失败了
                return 'GameOver'
        return 'Game'

    state_actions = {
        'Init': init,
        'Win': lambda: not_game('Win'),
        'GameOver': lambda: not_game('GameOver'),
        'Game': game
    }
    curses.use_default_colors()
    game_field = GameField(win=64)  # win设置为64做个例子

    state = 'Init'

    global scr  # 和打log有关
    scr = stdscr

    # 状态机开始循环
    while state != 'Exit':
        state = state_actions[state]()


stdscr = curses.initscr() # 和打log有关
# stdscr.scrollok(1) # 可以滚屏
curses.wrapper(main)
