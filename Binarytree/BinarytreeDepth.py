# class Node:
#     def __init__(self, data):
#         self.data = data
#         self.lchild = None
#         self.rchild = None
#
#
# class Tree:
#     def __init__(self):
#         self.root = None
#
#     def create(self, treedata):
#         try:
#             data = treedata.__next__()
#         except StopIteration:
#             return None
#
#         if data == ' ':
#             return None
#
#         node = Node(data)
#         if self.root is None:
#             self.root = node
#
#         node.lchild = self.create(treedata)
#         node.rchild = self.create(treedata)
#
#         return node
#
#     def traverse(self):
#         self.__traverse(self.root)
#
#     def __traverse(self, node):
#         if node is not None:
#             print(node.data)
#             self.__traverse(node.lchild)
#             self.__traverse(node.rchild)
#
#
# class TreeData:  # 封装
#     def __init__(self, lstdata):
#         self.data = lstdata
#
#     def getdata(self):
#         for data in self.data:
#             yield data
#
#
# if __name__ == '__main__':
#     lst = [1, 2, 3, 4]
#     dd = TreeData(lst)
#     a = dd.getdata()
#     print(a.__next__())
#     print(a.__next__())
#     print(a.__next__())
#     print(a.__next__())
#     # b = dd.getdata()
#     # print(b.__next__())
#     # print(b.__next__())
#     # print(b.__next__())
#     # print(b.__next__())
#
#     tree = Tree()
#     lstdata=[]
#     lstdata = input('Please input tree:')
#     treedata = TreeData(lstdata)
#     tree.create(treedata.getdata())
#     tree.traverse()

# OR

class Node:
    def __init__(self, data):
        self.data = data
        self.lchild = None
        self.rchild = None


class Tree:
    def __init__(self):
        self.root = None

    def getdata(self, lstdata):
        for data in lstdata:
            yield data

    def create(self, lstdataiter):
        try:
            data = lstdataiter.__next__()
        except StopIteration:
            return None

        if data == ' ':
            return None

        node = Node(data)
        if self.root is None:
            self.root = node

        node.lchild = self.create(lstdataiter)
        node.rchild = self.create(lstdataiter)

        return node

    def traverse(self, node):
        if node is not None:
            print(node.data)
            self.traverse(node.lchild)
            self.traverse(node.rchild)


    def traverse2(self, node):
        if node is None:
            return

        lst = [node]

        # while lst is not None and lst.count()!=0:
        while lst:
            cur = lst.pop(0)
            print(cur.data)
            if cur.lchild:
                lst.append(cur.lchild)
            if cur.rchild:
                lst.append(cur.rchild)



if __name__ == '__main__':
    tree = Tree()
    lstname = input('Please input data by preorder traversal:')
    print(type(lstname))

    node = tree.create(tree.getdata(lstname))
    # tree.traverse(node)
    tree.traverse2(node)
